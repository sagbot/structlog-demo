# structlog-demo

## Example logs

### GET /long
An http endpoint that sleeps for 3 seconds before returning the response. here is the source code:
```python
logger.info(f'This is a long request, it will take {settings.long_request_sleep_seconds} seconds')
start_time = time.perf_counter()
while time.perf_counter() < start_time + settings.long_request_sleep_seconds:
    logger.debug('Not done sleeping yet...')
    logging.getLogger(__name__).debug('Not done sleeping yet.. (log from stdlib)')
    sleep(0.5)
return f'request done after sleeping for {settings.long_request_sleep_seconds} seconds'
```
And this is the endpoints log output
```
2022-05-09T21:33:16.165450Z [debug    ] Using selector: KqueueSelector [asyncio] func_name=__init__ lineno=54
2022-05-09T21:33:16.172935Z [info     ] Started server process [74880] [uvicorn.error] func_name=serve lineno=75
2022-05-09T21:33:16.173120Z [info     ] Waiting for application startup. [uvicorn.error] func_name=startup lineno=45
2022-05-09T21:33:16.173494Z [info     ] Application startup complete.  [uvicorn.error] func_name=startup lineno=59
2022-05-09T21:33:16.174343Z [info     ] Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit) [uvicorn.error] func_name=_log_started_message lineno=206
2022-05-09T21:33:24.660978Z [info     ] Starting http request          [demo.middlewares.request_logging.middleware] client=127.0.0.1 func_name=add_log_context headers={'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36'} lineno=51 method=GET request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c uri=/long url=http://127.0.0.1:8000/long
2022-05-09T21:33:24.690610Z [info     ] This is a long request, it will take 3 seconds [demo.route] func_name=long_request lineno=19 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:24.691156Z [debug    ] Not done sleeping yet...       [demo.route] func_name=long_request lineno=22 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:24.691452Z [debug    ] Not done sleeping yet.. (log from stdlib) [demo.route] func_name=long_request lineno=23 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:25.197596Z [debug    ] Not done sleeping yet...       [demo.route] func_name=long_request lineno=22 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:25.199193Z [debug    ] Not done sleeping yet.. (log from stdlib) [demo.route] func_name=long_request lineno=23 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:25.701131Z [debug    ] Not done sleeping yet...       [demo.route] func_name=long_request lineno=22 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:25.702839Z [debug    ] Not done sleeping yet.. (log from stdlib) [demo.route] func_name=long_request lineno=23 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:26.205037Z [debug    ] Not done sleeping yet...       [demo.route] func_name=long_request lineno=22 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:26.206582Z [debug    ] Not done sleeping yet.. (log from stdlib) [demo.route] func_name=long_request lineno=23 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:26.712808Z [debug    ] Not done sleeping yet...       [demo.route] func_name=long_request lineno=22 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:26.715773Z [debug    ] Not done sleeping yet.. (log from stdlib) [demo.route] func_name=long_request lineno=23 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:27.219656Z [debug    ] Not done sleeping yet...       [demo.route] func_name=long_request lineno=22 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:27.220417Z [debug    ] Not done sleeping yet.. (log from stdlib) [demo.route] func_name=long_request lineno=23 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:27.727650Z [info     ] HTTP request complete          [demo.middlewares.request_logging.middleware] client=127.0.0.1 func_name=add_log_context headers={'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36'} lineno=86 method=GET request_duration=3.0378531669999997 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c response_status_code=200 uri=/long url=http://127.0.0.1:8000/long
2022-05-09T21:33:27.729819Z [info     ] 127.0.0.1:59715 - "GET /long HTTP/1.1" 200 [uvicorn.access] func_name=send lineno=431
```

### GET /bug
An http endpoint that throws a bug. This is meant to show you how your logs will look like if you have an unhandled exception in your code.
The source code is:
```python
logger.info('This code segment will rais an error')
result = 10 / 0
```
and the endpoints log output is:
```
2022-05-09T21:34:07.098287Z [debug    ] Using selector: KqueueSelector [asyncio] func_name=__init__ lineno=54
2022-05-09T21:34:07.104717Z [info     ] Started server process [74906] [uvicorn.error] func_name=serve lineno=75
2022-05-09T21:34:07.104862Z [info     ] Waiting for application startup. [uvicorn.error] func_name=startup lineno=45
2022-05-09T21:34:07.105028Z [info     ] Application startup complete.  [uvicorn.error] func_name=startup lineno=59
2022-05-09T21:34:07.105272Z [info     ] Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit) [uvicorn.error] func_name=_log_started_message lineno=206
2022-05-09T21:34:09.322698Z [info     ] Starting http request          [demo.middlewares.request_logging.middleware] client=127.0.0.1 func_name=add_log_context headers={'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36'} lineno=51 method=GET request_id=2f3d9dc5-efb7-4018-b237-44b49e9020f1 uri=/bug url=http://127.0.0.1:8000/bug
2022-05-09T21:34:09.348595Z [info     ] This code segment will rais an error [demo.route] func_name=bug_in_code lineno=36 request_id=2f3d9dc5-efb7-4018-b237-44b49e9020f1
2022-05-09T21:34:09.350577Z [info     ] HTTP request complete          [demo.middlewares.request_logging.middleware] client=127.0.0.1 exception_msg=division by zero exception_type=<class 'ZeroDivisionError'> func_name=add_log_context headers={'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36'} lineno=86 method=GET request_duration=0.0015931660000001457 request_id=2f3d9dc5-efb7-4018-b237-44b49e9020f1 uri=/bug url=http://127.0.0.1:8000/bug
Traceback (most recent call last):
  File "/opt/homebrew/lib/python3.9/site-packages/anyio/streams/memory.py", line 81, in receive
    return self.receive_nowait()
  File "/opt/homebrew/lib/python3.9/site-packages/anyio/streams/memory.py", line 76, in receive_nowait
    raise WouldBlock
anyio.WouldBlock

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/middleware/base.py", line 41, in call_next
    message = await recv_stream.receive()
  File "/opt/homebrew/lib/python3.9/site-packages/anyio/streams/memory.py", line 101, in receive
    raise EndOfStream
anyio.EndOfStream

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "/Users/sagivoulu/Projects/sagbot/structlog-demo/demo/middlewares/request_logging/middleware.py", line 63, in add_log_context
    response = await call_next(request)
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/middleware/base.py", line 44, in call_next
    raise app_exc
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/middleware/base.py", line 34, in coro
    await self.app(scope, request.receive, send_stream.send)
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/exceptions.py", line 82, in __call__
    raise exc
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/exceptions.py", line 71, in __call__
    await self.app(scope, receive, sender)
  File "/opt/homebrew/lib/python3.9/site-packages/fastapi/middleware/asyncexitstack.py", line 21, in __call__
    raise e
  File "/opt/homebrew/lib/python3.9/site-packages/fastapi/middleware/asyncexitstack.py", line 18, in __call__
    await self.app(scope, receive, send)
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/routing.py", line 656, in __call__
    await route.handle(scope, receive, send)
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/routing.py", line 259, in handle
    await self.app(scope, receive, send)
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/routing.py", line 61, in app
    response = await func(request)
  File "/opt/homebrew/lib/python3.9/site-packages/fastapi/routing.py", line 227, in app
    raw_response = await run_endpoint_function(
  File "/opt/homebrew/lib/python3.9/site-packages/fastapi/routing.py", line 162, in run_endpoint_function
    return await run_in_threadpool(dependant.call, **values)
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/concurrency.py", line 39, in run_in_threadpool
    return await anyio.to_thread.run_sync(func, *args)
  File "/opt/homebrew/lib/python3.9/site-packages/anyio/to_thread.py", line 28, in run_sync
    return await get_asynclib().run_sync_in_worker_thread(func, *args, cancellable=cancellable,
  File "/opt/homebrew/lib/python3.9/site-packages/anyio/_backends/_asyncio.py", line 818, in run_sync_in_worker_thread
    return await future
  File "/opt/homebrew/lib/python3.9/site-packages/anyio/_backends/_asyncio.py", line 754, in run
    result = context.run(func, *args)
  File "/Users/sagivoulu/Projects/sagbot/structlog-demo/demo/route.py", line 37, in bug_in_code
    result = 10 / 0
ZeroDivisionError: division by zero

2022-05-09T21:34:09.351206Z [info     ] 127.0.0.1:59731 - "GET /bug HTTP/1.1" 500 [uvicorn.access] func_name=send lineno=431
2022-05-09T21:34:09.351618Z [error    ] Exception in ASGI application
 [uvicorn.error] func_name=run_asgi lineno=369
Traceback (most recent call last):
  File "/opt/homebrew/lib/python3.9/site-packages/uvicorn/protocols/http/h11_impl.py", line 366, in run_asgi
    result = await app(self.scope, self.receive, self.send)
  File "/opt/homebrew/lib/python3.9/site-packages/uvicorn/middleware/proxy_headers.py", line 75, in __call__
    return await self.app(scope, receive, send)
  File "/opt/homebrew/lib/python3.9/site-packages/uvicorn/middleware/message_logger.py", line 82, in __call__
    raise exc from None
  File "/opt/homebrew/lib/python3.9/site-packages/uvicorn/middleware/message_logger.py", line 78, in __call__
    await self.app(scope, inner_receive, inner_send)
  File "/opt/homebrew/lib/python3.9/site-packages/fastapi/applications.py", line 261, in __call__
    await super().__call__(scope, receive, send)
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/applications.py", line 112, in __call__
    await self.middleware_stack(scope, receive, send)
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/middleware/errors.py", line 181, in __call__
    raise exc
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/middleware/errors.py", line 159, in __call__
    await self.app(scope, receive, _send)
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/middleware/base.py", line 63, in __call__
    response = await self.dispatch_func(request, call_next)
  File "/Users/sagivoulu/Projects/sagbot/structlog-demo/demo/middlewares/request_logging/middleware.py", line 63, in add_log_context
    response = await call_next(request)
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/middleware/base.py", line 44, in call_next
    raise app_exc
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/middleware/base.py", line 34, in coro
    await self.app(scope, request.receive, send_stream.send)
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/exceptions.py", line 82, in __call__
    raise exc
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/exceptions.py", line 71, in __call__
    await self.app(scope, receive, sender)
  File "/opt/homebrew/lib/python3.9/site-packages/fastapi/middleware/asyncexitstack.py", line 21, in __call__
    raise e
  File "/opt/homebrew/lib/python3.9/site-packages/fastapi/middleware/asyncexitstack.py", line 18, in __call__
    await self.app(scope, receive, send)
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/routing.py", line 656, in __call__
    await route.handle(scope, receive, send)
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/routing.py", line 259, in handle
    await self.app(scope, receive, send)
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/routing.py", line 61, in app
    response = await func(request)
  File "/opt/homebrew/lib/python3.9/site-packages/fastapi/routing.py", line 227, in app
    raw_response = await run_endpoint_function(
  File "/opt/homebrew/lib/python3.9/site-packages/fastapi/routing.py", line 162, in run_endpoint_function
    return await run_in_threadpool(dependant.call, **values)
  File "/opt/homebrew/lib/python3.9/site-packages/starlette/concurrency.py", line 39, in run_in_threadpool
    return await anyio.to_thread.run_sync(func, *args)
  File "/opt/homebrew/lib/python3.9/site-packages/anyio/to_thread.py", line 28, in run_sync
    return await get_asynclib().run_sync_in_worker_thread(func, *args, cancellable=cancellable,
  File "/opt/homebrew/lib/python3.9/site-packages/anyio/_backends/_asyncio.py", line 818, in run_sync_in_worker_thread
    return await future
  File "/opt/homebrew/lib/python3.9/site-packages/anyio/_backends/_asyncio.py", line 754, in run
    result = context.run(func, *args)
  File "/Users/sagivoulu/Projects/sagbot/structlog-demo/demo/route.py", line 37, in bug_in_code
    result = 10 / 0
ZeroDivisionError: division by zero
```
