"""
A demo FastAPI Application demonstrating usage of structured logs.

## Enough chit chat, show me the logs!
The logs look somthing like this:
```
2022-05-09T21:33:16.165450Z [debug    ] Using selector: KqueueSelector [asyncio] func_name=__init__ lineno=54
2022-05-09T21:33:16.172935Z [info     ] Started server process [74880] [uvicorn.error] func_name=serve lineno=75
2022-05-09T21:33:16.173120Z [info     ] Waiting for application startup. [uvicorn.error] func_name=startup lineno=45
2022-05-09T21:33:16.173494Z [info     ] Application startup complete.  [uvicorn.error] func_name=startup lineno=59
2022-05-09T21:33:16.174343Z [info     ] Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit) [uvicorn.error] func_name=_log_started_message lineno=206
2022-05-09T21:33:24.660978Z [info     ] Starting http request          [demo.middlewares.request_logging.middleware] client=127.0.0.1 func_name=add_log_context headers={'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36'} lineno=51 method=GET request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c uri=/long url=http://127.0.0.1:8000/long
2022-05-09T21:33:24.690610Z [info     ] This is a long request, it will take 3 seconds [demo.route] func_name=long_request lineno=19 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:24.691156Z [debug    ] Not done sleeping yet...       [demo.route] func_name=long_request lineno=22 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:24.691452Z [debug    ] Not done sleeping yet.. (log from stdlib) [demo.route] func_name=long_request lineno=23 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:25.197596Z [debug    ] Not done sleeping yet...       [demo.route] func_name=long_request lineno=22 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:25.199193Z [debug    ] Not done sleeping yet.. (log from stdlib) [demo.route] func_name=long_request lineno=23 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:25.701131Z [debug    ] Not done sleeping yet...       [demo.route] func_name=long_request lineno=22 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:25.702839Z [debug    ] Not done sleeping yet.. (log from stdlib) [demo.route] func_name=long_request lineno=23 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:26.205037Z [debug    ] Not done sleeping yet...       [demo.route] func_name=long_request lineno=22 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:26.206582Z [debug    ] Not done sleeping yet.. (log from stdlib) [demo.route] func_name=long_request lineno=23 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:26.712808Z [debug    ] Not done sleeping yet...       [demo.route] func_name=long_request lineno=22 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:26.715773Z [debug    ] Not done sleeping yet.. (log from stdlib) [demo.route] func_name=long_request lineno=23 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:27.219656Z [debug    ] Not done sleeping yet...       [demo.route] func_name=long_request lineno=22 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:27.220417Z [debug    ] Not done sleeping yet.. (log from stdlib) [demo.route] func_name=long_request lineno=23 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c
2022-05-09T21:33:27.727650Z [info     ] HTTP request complete          [demo.middlewares.request_logging.middleware] client=127.0.0.1 func_name=add_log_context headers={'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36'} lineno=86 method=GET request_duration=3.0378531669999997 request_id=0a4b0ad3-aab9-4429-a50b-1f20775eff8c response_status_code=200 uri=/long url=http://127.0.0.1:8000/long
2022-05-09T21:33:27.729819Z [info     ] 127.0.0.1:59715 - "GET /long HTTP/1.1" 200 [uvicorn.access] func_name=send lineno=431
```
"""